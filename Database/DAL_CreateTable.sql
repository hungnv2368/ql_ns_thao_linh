/*
Run this script on:

        localhost.QLNhanSu_TrienKhai    -  This database will be modified

to synchronize it with:

        localhost.QLNhanSu

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.1.0 from Red Gate Software Ltd at 10/21/2017 2:38:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChamCong]'
GO
CREATE TABLE [dbo].[ChamCong]
(
[ChamCongID] [bigint] NOT NULL IDENTITY(1, 1),
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LuongCoBan] [decimal] (22, 6) NULL,
[LuongTheoNgayCong] [decimal] (22, 6) NULL,
[Nam] [int] NULL,
[NhanVienID] [bigint] NOT NULL,
[SoNgayCong] [decimal] (22, 6) NOT NULL,
[SoNgayNghi] [int] NULL,
[TamUng] [decimal] (22, 6) NULL,
[Thang] [int] NULL,
[ThucLinh] [decimal] (22, 6) NULL,
[TongPhuCap] [decimal] (22, 6) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_ChamCong] on [dbo].[ChamCong]'
GO
ALTER TABLE [dbo].[ChamCong] ADD CONSTRAINT [PK_ChamCong] PRIMARY KEY CLUSTERED  ([ChamCongID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChamCong_Insert]'
GO
CREATE PROCEDURE [dbo].[ChamCong_Insert]
	@ChamCongID bigint output,
	@GhiChu nvarchar(500) = null ,
	@LuongCoBan decimal(22,6) = null ,
	@LuongTheoNgayCong decimal(22,6) = null ,
	@Nam int = null ,
	@NhanVienID bigint ,
	@SoNgayCong decimal(22,6) ,
	@SoNgayNghi int = null ,
	@TamUng decimal(22,6) = null ,
	@Thang int = null ,
	@ThucLinh decimal(22,6) = null ,
	@TongPhuCap decimal(22,6) = null 

AS

INSERT [dbo].[ChamCong]
(
	[GhiChu],
	[LuongCoBan],
	[LuongTheoNgayCong],
	[Nam],
	[NhanVienID],
	[SoNgayCong],
	[SoNgayNghi],
	[TamUng],
	[Thang],
	[ThucLinh],
	[TongPhuCap]

)
VALUES
(
	@GhiChu,
	@LuongCoBan,
	@LuongTheoNgayCong,
	@Nam,
	@NhanVienID,
	@SoNgayCong,
	@SoNgayNghi,
	@TamUng,
	@Thang,
	@ThucLinh,
	@TongPhuCap

)
	SELECT @ChamCongID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChamCong_Update]'
GO
CREATE PROCEDURE [dbo].[ChamCong_Update]
	@ChamCongID bigint,
	@GhiChu nvarchar(500) = null,
	@LuongCoBan decimal(22,6) = null,
	@LuongTheoNgayCong decimal(22,6) = null,
	@Nam int = null,
	@NhanVienID bigint,
	@SoNgayCong decimal(22,6),
	@SoNgayNghi int = null,
	@TamUng decimal(22,6) = null,
	@Thang int = null,
	@ThucLinh decimal(22,6) = null,
	@TongPhuCap decimal(22,6) = null

AS

UPDATE [dbo].[ChamCong]
SET
	[GhiChu] = @GhiChu,
	[LuongCoBan] = @LuongCoBan,
	[LuongTheoNgayCong] = @LuongTheoNgayCong,
	[Nam] = @Nam,
	[NhanVienID] = @NhanVienID,
	[SoNgayCong] = @SoNgayCong,
	[SoNgayNghi] = @SoNgayNghi,
	[TamUng] = @TamUng,
	[Thang] = @Thang,
	[ThucLinh] = @ThucLinh,
	[TongPhuCap] = @TongPhuCap
 WHERE 
	[ChamCongID] = @ChamCongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChamCong_SelectAll]'
GO
CREATE PROCEDURE [dbo].[ChamCong_SelectAll]
AS

	SELECT 
		[ChamCongID], [GhiChu], [LuongCoBan], [LuongTheoNgayCong], [Nam], [NhanVienID], [SoNgayCong], [SoNgayNghi], [TamUng], [Thang], [ThucLinh], [TongPhuCap]
	FROM [dbo].[ChamCong]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChamCong_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[ChamCong_DeleteByPrimaryKey]
	@ChamCongID bigint
AS

DELETE FROM [dbo].[ChamCong]
 WHERE 
	[ChamCongID] = @ChamCongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVien]'
GO
CREATE TABLE [dbo].[NhanVien]
(
[BiDanh] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChucVuID] [int] NULL,
[DonViID] [int] NULL,
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GioiTinh] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoDem] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImagesPersonal] [varbinary] (1) NULL,
[MaNV] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgaySinh] [datetime] NULL,
[NhanVienID] [bigint] NOT NULL IDENTITY(1, 1),
[Ten] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NhanVien] on [dbo].[NhanVien]'
GO
ALTER TABLE [dbo].[NhanVien] ADD CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED  ([NhanVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChucVu]'
GO
CREATE TABLE [dbo].[ChucVu]
(
[ChucVuID] [int] NOT NULL IDENTITY(1, 1),
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaChucVu] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenChucVu] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_ChucVu] on [dbo].[ChucVu]'
GO
ALTER TABLE [dbo].[ChucVu] ADD CONSTRAINT [PK_ChucVu] PRIMARY KEY CLUSTERED  ([ChucVuID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienFullView]'
GO
CREATE VIEW [dbo].[NhanVienFullView] AS 
SELECT     dbo.NhanVien.BiDanh, dbo.NhanVien.ChucVuID, dbo.NhanVien.DonViID, dbo.NhanVien.GhiChu, dbo.NhanVien.GioiTinh, dbo.NhanVien.HoDem, 
                      dbo.NhanVien.ImagesPersonal, dbo.NhanVien.MaNV, dbo.NhanVien.NgaySinh, dbo.NhanVien.NhanVienID, dbo.NhanVien.Ten, dbo.ChucVu.TenChucVu, 
                      dbo.ChucVu.MaChucVu, CONCAT(CONCAT(dbo.NhanVien.HoDem, ' '),dbo.NhanVien.Ten) AS 'HoVaTen'
FROM         dbo.NhanVien LEFT OUTER JOIN
                      dbo.ChucVu ON dbo.NhanVien.ChucVuID = dbo.ChucVu.ChucVuID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChucVu_Insert]'
GO
CREATE PROCEDURE [dbo].[ChucVu_Insert]
	@ChucVuID int output,
	@GhiChu nvarchar(500) = null ,
	@MaChucVu nvarchar(50) = null ,
	@TenChucVu nvarchar(200) = null 

AS

INSERT [dbo].[ChucVu]
(
	[GhiChu],
	[MaChucVu],
	[TenChucVu]

)
VALUES
(
	@GhiChu,
	@MaChucVu,
	@TenChucVu

)
	SELECT @ChucVuID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChucVu_Update]'
GO
CREATE PROCEDURE [dbo].[ChucVu_Update]
	@ChucVuID int,
	@GhiChu nvarchar(500) = null,
	@MaChucVu nvarchar(50) = null,
	@TenChucVu nvarchar(200) = null

AS

UPDATE [dbo].[ChucVu]
SET
	[GhiChu] = @GhiChu,
	[MaChucVu] = @MaChucVu,
	[TenChucVu] = @TenChucVu
 WHERE 
	[ChucVuID] = @ChucVuID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChucVu_SelectAll]'
GO
CREATE PROCEDURE [dbo].[ChucVu_SelectAll]
AS

	SELECT 
		[ChucVuID], [GhiChu], [MaChucVu], [TenChucVu]
	FROM [dbo].[ChucVu]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ChucVu_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[ChucVu_DeleteByPrimaryKey]
	@ChucVuID int
AS

DELETE FROM [dbo].[ChucVu]
 WHERE 
	[ChucVuID] = @ChucVuID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DanToc]'
GO
CREATE TABLE [dbo].[DanToc]
(
[DanTocID] [int] NOT NULL IDENTITY(1, 1),
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDanToc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_DanToc] on [dbo].[DanToc]'
GO
ALTER TABLE [dbo].[DanToc] ADD CONSTRAINT [PK_DanToc] PRIMARY KEY CLUSTERED  ([DanTocID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DanToc_Insert]'
GO
CREATE PROCEDURE [dbo].[DanToc_Insert]
	@DanTocID int output,
	@GhiChu nvarchar(500) = null ,
	@TenDanToc nvarchar(50) = null 

AS

INSERT [dbo].[DanToc]
(
	[GhiChu],
	[TenDanToc]

)
VALUES
(
	@GhiChu,
	@TenDanToc

)
	SELECT @DanTocID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DonVi]'
GO
CREATE TABLE [dbo].[DonVi]
(
[DonViID] [int] NOT NULL IDENTITY(1, 1),
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonVi] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayTLap] [datetime] NULL,
[TenDonVi] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChuyenNganh] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoThanhVien] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_DonVi] on [dbo].[DonVi]'
GO
ALTER TABLE [dbo].[DonVi] ADD CONSTRAINT [PK_DonVi] PRIMARY KEY CLUSTERED  ([DonViID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LuongFullView]'
GO
CREATE VIEW [dbo].[LuongFullView] AS 
SELECT
dbo.ChamCong.Nam,
dbo.ChamCong.Thang,
dbo.ChamCong.NhanVienID,
dbo.NhanVien.MaNV,
NhanVien.DonViID,
dbo.DonVi.TenDonVi,
CONCAT(CONCAT(dbo.NhanVien.HoDem, ' '),dbo.NhanVien.Ten) AS 'HoVaTen',
dbo.NhanVien.NgaySinh,
dbo.ChamCong.LuongCoBan,
dbo.ChamCong.ThucLinh,
ChamCong.TamUng,
dbo.ChamCong.TongPhuCap
FROM
dbo.ChamCong LEFT JOIN Nhanvien on ChamCong.NhanVienID=NhanVien.NhanVienID
LEFT JOIN DonVi on NhanVien.DonViID=DonVi.DonViID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DanToc_Update]'
GO
CREATE PROCEDURE [dbo].[DanToc_Update]
	@DanTocID int,
	@GhiChu nvarchar(500) = null,
	@TenDanToc nvarchar(50) = null

AS

UPDATE [dbo].[DanToc]
SET
	[GhiChu] = @GhiChu,
	[TenDanToc] = @TenDanToc
 WHERE 
	[DanTocID] = @DanTocID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DanToc_SelectAll]'
GO
CREATE PROCEDURE [dbo].[DanToc_SelectAll]
AS

	SELECT 
		[DanTocID], [GhiChu], [TenDanToc]
	FROM [dbo].[DanToc]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DanToc_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[DanToc_DeleteByPrimaryKey]
	@DanTocID int
AS

DELETE FROM [dbo].[DanToc]
 WHERE 
	[DanTocID] = @DanTocID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HocVan]'
GO
CREATE TABLE [dbo].[HocVan]
(
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HocVanID] [int] NOT NULL IDENTITY(1, 1),
[TenHocVan] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_HocVan] on [dbo].[HocVan]'
GO
ALTER TABLE [dbo].[HocVan] ADD CONSTRAINT [PK_HocVan] PRIMARY KEY CLUSTERED  ([HocVanID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HocVan_Insert]'
GO
CREATE PROCEDURE [dbo].[HocVan_Insert]
	@GhiChu nvarchar(500) = null ,
	@HocVanID int output,
	@TenHocVan nvarchar(200) = null 

AS

INSERT [dbo].[HocVan]
(
	[GhiChu],
	[TenHocVan]

)
VALUES
(
	@GhiChu,
	@TenHocVan

)
	SELECT @HocVanID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HocVan_Update]'
GO
CREATE PROCEDURE [dbo].[HocVan_Update]
	@GhiChu nvarchar(500) = null,
	@HocVanID int,
	@TenHocVan nvarchar(200) = null

AS

UPDATE [dbo].[HocVan]
SET
	[GhiChu] = @GhiChu,
	[TenHocVan] = @TenHocVan
 WHERE 
	[HocVanID] = @HocVanID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HocVan_SelectAll]'
GO
CREATE PROCEDURE [dbo].[HocVan_SelectAll]
AS

	SELECT 
		[GhiChu], [HocVanID], [TenHocVan]
	FROM [dbo].[HocVan]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HocVan_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[HocVan_DeleteByPrimaryKey]
	@HocVanID int
AS

DELETE FROM [dbo].[HocVan]
 WHERE 
	[HocVanID] = @HocVanID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LamThemGio]'
GO
CREATE TABLE [dbo].[LamThemGio]
(
[BatDau] [datetime] NOT NULL,
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KetThuc] [datetime] NOT NULL,
[LamThemGioID] [bigint] NOT NULL IDENTITY(1, 1),
[NgayLam] [datetime] NOT NULL,
[NhanVienID] [bigint] NOT NULL,
[SoGio] [decimal] (22, 6) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_LamThemGio] on [dbo].[LamThemGio]'
GO
ALTER TABLE [dbo].[LamThemGio] ADD CONSTRAINT [PK_LamThemGio] PRIMARY KEY CLUSTERED  ([LamThemGioID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LamThemGio_Insert]'
GO
CREATE PROCEDURE [dbo].[LamThemGio_Insert]
	@BatDau datetime ,
	@GhiChu nvarchar(500) = null ,
	@KetThuc datetime ,
	@LamThemGioID bigint output,
	@NgayLam datetime ,
	@NhanVienID bigint ,
	@SoGio decimal(22,6) = null 

AS

INSERT [dbo].[LamThemGio]
(
	[BatDau],
	[GhiChu],
	[KetThuc],
	[NgayLam],
	[NhanVienID],
	[SoGio]

)
VALUES
(
	@BatDau,
	@GhiChu,
	@KetThuc,
	@NgayLam,
	@NhanVienID,
	@SoGio

)
	SELECT @LamThemGioID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LamThemGio_Update]'
GO
CREATE PROCEDURE [dbo].[LamThemGio_Update]
	@BatDau datetime,
	@GhiChu nvarchar(500) = null,
	@KetThuc datetime,
	@LamThemGioID bigint,
	@NgayLam datetime,
	@NhanVienID bigint,
	@SoGio decimal(22,6) = null

AS

UPDATE [dbo].[LamThemGio]
SET
	[BatDau] = @BatDau,
	[GhiChu] = @GhiChu,
	[KetThuc] = @KetThuc,
	[NgayLam] = @NgayLam,
	[NhanVienID] = @NhanVienID,
	[SoGio] = @SoGio
 WHERE 
	[LamThemGioID] = @LamThemGioID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LamThemGio_SelectAll]'
GO
CREATE PROCEDURE [dbo].[LamThemGio_SelectAll]
AS

	SELECT 
		[BatDau], [GhiChu], [KetThuc], [LamThemGioID], [NgayLam], [NhanVienID], [SoGio]
	FROM [dbo].[LamThemGio]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LamThemGio_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[LamThemGio_DeleteByPrimaryKey]
	@LamThemGioID bigint
AS

DELETE FROM [dbo].[LamThemGio]
 WHERE 
	[LamThemGioID] = @LamThemGioID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoaiHopDong]'
GO
CREATE TABLE [dbo].[LoaiHopDong]
(
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KyhieuHD] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiHopDongID] [bigint] NOT NULL IDENTITY(1, 1),
[TenLoaiHopDong] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThoiGian] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_LoaiHopDong] on [dbo].[LoaiHopDong]'
GO
ALTER TABLE [dbo].[LoaiHopDong] ADD CONSTRAINT [PK_LoaiHopDong] PRIMARY KEY CLUSTERED  ([LoaiHopDongID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoaiHopDong_Insert]'
GO
CREATE PROCEDURE [dbo].[LoaiHopDong_Insert]
	@GhiChu nvarchar(500) = null ,
	@KyhieuHD nvarchar(50) = null ,
	@LoaiHopDongID bigint output,
	@TenLoaiHopDong nvarchar(100) = null ,
	@ThoiGian nvarchar(200) = null 

AS

INSERT [dbo].[LoaiHopDong]
(
	[GhiChu],
	[KyhieuHD],
	[TenLoaiHopDong],
	[ThoiGian]

)
VALUES
(
	@GhiChu,
	@KyhieuHD,
	@TenLoaiHopDong,
	@ThoiGian

)
	SELECT @LoaiHopDongID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DonVi_Insert]'
GO
CREATE PROCEDURE [dbo].[DonVi_Insert]
	@DonViID int output,
	@GhiChu nvarchar(500) = null ,
	@MaDonVi nvarchar(50) = null ,
	@NgayTLap datetime = null ,
	@TenDonVi nvarchar(200) = null ,
	@ChuyenNganh nvarchar(200) = null ,
	@SoThanhVien int = null 

AS

INSERT [dbo].[DonVi]
(
	[GhiChu],
	[MaDonVi],
	[NgayTLap],
	[TenDonVi],
	[ChuyenNganh],
	[SoThanhVien]

)
VALUES
(
	@GhiChu,
	@MaDonVi,
	@NgayTLap,
	@TenDonVi,
	@ChuyenNganh,
	@SoThanhVien

)
	SELECT @DonViID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoaiHopDong_Update]'
GO
CREATE PROCEDURE [dbo].[LoaiHopDong_Update]
	@GhiChu nvarchar(500) = null,
	@KyhieuHD nvarchar(50) = null,
	@LoaiHopDongID bigint,
	@TenLoaiHopDong nvarchar(100) = null,
	@ThoiGian nvarchar(200) = null

AS

UPDATE [dbo].[LoaiHopDong]
SET
	[GhiChu] = @GhiChu,
	[KyhieuHD] = @KyhieuHD,
	[TenLoaiHopDong] = @TenLoaiHopDong,
	[ThoiGian] = @ThoiGian
 WHERE 
	[LoaiHopDongID] = @LoaiHopDongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DonVi_Update]'
GO
CREATE PROCEDURE [dbo].[DonVi_Update]
	@DonViID int,
	@GhiChu nvarchar(500) = null,
	@MaDonVi nvarchar(50) = null,
	@NgayTLap datetime = null,
	@TenDonVi nvarchar(200) = null,
	@ChuyenNganh nvarchar(200) = null,
	@SoThanhVien int = null

AS

UPDATE [dbo].[DonVi]
SET
	[GhiChu] = @GhiChu,
	[MaDonVi] = @MaDonVi,
	[NgayTLap] = @NgayTLap,
	[TenDonVi] = @TenDonVi,
	[ChuyenNganh] = @ChuyenNganh,
	[SoThanhVien] = @SoThanhVien
 WHERE 
	[DonViID] = @DonViID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoaiHopDong_SelectAll]'
GO
CREATE PROCEDURE [dbo].[LoaiHopDong_SelectAll]
AS

	SELECT 
		[GhiChu], [KyhieuHD], [LoaiHopDongID], [TenLoaiHopDong], [ThoiGian]
	FROM [dbo].[LoaiHopDong]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DonVi_SelectAll]'
GO
CREATE PROCEDURE [dbo].[DonVi_SelectAll]
AS

	SELECT 
		[DonViID], [GhiChu], [MaDonVi], [NgayTLap], [TenDonVi], [ChuyenNganh], [SoThanhVien]
	FROM [dbo].[DonVi]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoaiHopDong_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[LoaiHopDong_DeleteByPrimaryKey]
	@LoaiHopDongID bigint
AS

DELETE FROM [dbo].[LoaiHopDong]
 WHERE 
	[LoaiHopDongID] = @LoaiHopDongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DonVi_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[DonVi_DeleteByPrimaryKey]
	@DonViID int
AS

DELETE FROM [dbo].[DonVi]
 WHERE 
	[DonViID] = @DonViID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NgoaiNgu]'
GO
CREATE TABLE [dbo].[NgoaiNgu]
(
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgoaiNguID] [int] NOT NULL IDENTITY(1, 1),
[TenNgoaiNgu] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NgoaiNgu] on [dbo].[NgoaiNgu]'
GO
ALTER TABLE [dbo].[NgoaiNgu] ADD CONSTRAINT [PK_NgoaiNgu] PRIMARY KEY CLUSTERED  ([NgoaiNguID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NgoaiNgu_Insert]'
GO
CREATE PROCEDURE [dbo].[NgoaiNgu_Insert]
	@GhiChu nvarchar(500) = null ,
	@NgoaiNguID int output,
	@TenNgoaiNgu nvarchar(50) = null 

AS

INSERT [dbo].[NgoaiNgu]
(
	[GhiChu],
	[TenNgoaiNgu]

)
VALUES
(
	@GhiChu,
	@TenNgoaiNgu

)
	SELECT @NgoaiNguID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienDetail]'
GO
CREATE TABLE [dbo].[NhanVienDetail]
(
[CMTND] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DCTamTru] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DCThuongTru] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DanTocID] [int] NULL,
[DienThoai] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HocVanID] [int] NULL,
[LoaiHopDongID] [int] NULL,
[NgayCap] [datetime] NULL,
[NgoaiNguID] [int] NULL,
[NguyenQuan] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhanVienID] [bigint] NOT NULL,
[NoiCap] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiSinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuocTich] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TTHonNhan] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TonGiaoID] [int] NULL,
[NgayVaoDang] [datetime] NULL,
[NgayVaoTruong] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NhanVienDetail] on [dbo].[NhanVienDetail]'
GO
ALTER TABLE [dbo].[NhanVienDetail] ADD CONSTRAINT [PK_NhanVienDetail] PRIMARY KEY CLUSTERED  ([NhanVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienDetail_Insert]'
GO
CREATE PROCEDURE [dbo].[NhanVienDetail_Insert]
	@CMTND varchar(12) = null ,
	@DCTamTru nvarchar(200) = null ,
	@DCThuongTru nvarchar(200) = null ,
	@DanTocID int = null ,
	@DienThoai nvarchar(20) = null ,
	@Email nvarchar(200) = null ,
	@HocVanID int = null ,
	@LoaiHopDongID int = null ,
	@NgayCap datetime = null ,
	@NgoaiNguID int = null ,
	@NguyenQuan nvarchar(200) = null ,
	@NhanVienID bigint ,
	@NoiCap nvarchar(500) = null ,
	@NoiSinh nvarchar(50) = null ,
	@QuocTich nvarchar(200) = null ,
	@TTHonNhan nvarchar(50) = null ,
	@TonGiaoID int = null ,
	@NgayVaoDang datetime = null ,
	@NgayVaoTruong datetime = null 

AS

INSERT [dbo].[NhanVienDetail]
(
	[CMTND],
	[DCTamTru],
	[DCThuongTru],
	[DanTocID],
	[DienThoai],
	[Email],
	[HocVanID],
	[LoaiHopDongID],
	[NgayCap],
	[NgoaiNguID],
	[NguyenQuan],
	[NhanVienID],
	[NoiCap],
	[NoiSinh],
	[QuocTich],
	[TTHonNhan],
	[TonGiaoID],
	[NgayVaoDang],
	[NgayVaoTruong]

)
VALUES
(
	@CMTND,
	@DCTamTru,
	@DCThuongTru,
	@DanTocID,
	@DienThoai,
	@Email,
	@HocVanID,
	@LoaiHopDongID,
	@NgayCap,
	@NgoaiNguID,
	@NguyenQuan,
	@NhanVienID,
	@NoiCap,
	@NoiSinh,
	@QuocTich,
	@TTHonNhan,
	@TonGiaoID,
	@NgayVaoDang,
	@NgayVaoTruong

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NgoaiNgu_Update]'
GO
CREATE PROCEDURE [dbo].[NgoaiNgu_Update]
	@GhiChu nvarchar(500) = null,
	@NgoaiNguID int,
	@TenNgoaiNgu nvarchar(50) = null

AS

UPDATE [dbo].[NgoaiNgu]
SET
	[GhiChu] = @GhiChu,
	[TenNgoaiNgu] = @TenNgoaiNgu
 WHERE 
	[NgoaiNguID] = @NgoaiNguID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienDetail_Update]'
GO
CREATE PROCEDURE [dbo].[NhanVienDetail_Update]
	@CMTND varchar(12) = null,
	@DCTamTru nvarchar(200) = null,
	@DCThuongTru nvarchar(200) = null,
	@DanTocID int = null,
	@DienThoai nvarchar(20) = null,
	@Email nvarchar(200) = null,
	@HocVanID int = null,
	@LoaiHopDongID int = null,
	@NgayCap datetime = null,
	@NgoaiNguID int = null,
	@NguyenQuan nvarchar(200) = null,
	@NhanVienID bigint,
	@NoiCap nvarchar(500) = null,
	@NoiSinh nvarchar(50) = null,
	@QuocTich nvarchar(200) = null,
	@TTHonNhan nvarchar(50) = null,
	@TonGiaoID int = null,
	@NgayVaoDang datetime = null,
	@NgayVaoTruong datetime = null

AS

UPDATE [dbo].[NhanVienDetail]
SET
	[CMTND] = @CMTND,
	[DCTamTru] = @DCTamTru,
	[DCThuongTru] = @DCThuongTru,
	[DanTocID] = @DanTocID,
	[DienThoai] = @DienThoai,
	[Email] = @Email,
	[HocVanID] = @HocVanID,
	[LoaiHopDongID] = @LoaiHopDongID,
	[NgayCap] = @NgayCap,
	[NgoaiNguID] = @NgoaiNguID,
	[NguyenQuan] = @NguyenQuan,
	[NhanVienID] = @NhanVienID,
	[NoiCap] = @NoiCap,
	[NoiSinh] = @NoiSinh,
	[QuocTich] = @QuocTich,
	[TTHonNhan] = @TTHonNhan,
	[TonGiaoID] = @TonGiaoID,
	[NgayVaoDang] = @NgayVaoDang,
	[NgayVaoTruong] = @NgayVaoTruong
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NgoaiNgu_SelectAll]'
GO
CREATE PROCEDURE [dbo].[NgoaiNgu_SelectAll]
AS

	SELECT 
		[GhiChu], [NgoaiNguID], [TenNgoaiNgu]
	FROM [dbo].[NgoaiNgu]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienDetail_SelectAll]'
GO
CREATE PROCEDURE [dbo].[NhanVienDetail_SelectAll]
AS

	SELECT 
		[CMTND], [DCTamTru], [DCThuongTru], [DanTocID], [DienThoai], [Email], [HocVanID], [LoaiHopDongID], [NgayCap], [NgoaiNguID], [NguyenQuan], [NhanVienID], [NoiCap], [NoiSinh], [QuocTich], [TTHonNhan], [TonGiaoID], [NgayVaoDang], [NgayVaoTruong]
	FROM [dbo].[NhanVienDetail]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NgoaiNgu_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[NgoaiNgu_DeleteByPrimaryKey]
	@NgoaiNguID int
AS

DELETE FROM [dbo].[NgoaiNgu]
 WHERE 
	[NgoaiNguID] = @NgoaiNguID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienDetail_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[NhanVienDetail_DeleteByPrimaryKey]
	@NhanVienID bigint
AS

DELETE FROM [dbo].[NhanVienDetail]
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienLogin]'
GO
CREATE TABLE [dbo].[NhanVienLogin]
(
[CountFail] [int] NULL,
[LastedLogin] [datetime] NULL,
[NhanVienID] [bigint] NOT NULL,
[Password] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NhanVienLogin] on [dbo].[NhanVienLogin]'
GO
ALTER TABLE [dbo].[NhanVienLogin] ADD CONSTRAINT [PK_NhanVienLogin] PRIMARY KEY CLUSTERED  ([NhanVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienLogin_Insert]'
GO
CREATE PROCEDURE [dbo].[NhanVienLogin_Insert]
	@CountFail int = null ,
	@LastedLogin datetime = null ,
	@NhanVienID bigint ,
	@Password nvarchar(200) = null ,
	@UserName nvarchar(50) = null 

AS

INSERT [dbo].[NhanVienLogin]
(
	[CountFail],
	[LastedLogin],
	[NhanVienID],
	[Password],
	[UserName]

)
VALUES
(
	@CountFail,
	@LastedLogin,
	@NhanVienID,
	@Password,
	@UserName

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienLogin_Update]'
GO
CREATE PROCEDURE [dbo].[NhanVienLogin_Update]
	@CountFail int = null,
	@LastedLogin datetime = null,
	@NhanVienID bigint,
	@Password nvarchar(200) = null,
	@UserName nvarchar(50) = null

AS

UPDATE [dbo].[NhanVienLogin]
SET
	[CountFail] = @CountFail,
	[LastedLogin] = @LastedLogin,
	[NhanVienID] = @NhanVienID,
	[Password] = @Password,
	[UserName] = @UserName
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienLogin_SelectAll]'
GO
CREATE PROCEDURE [dbo].[NhanVienLogin_SelectAll]
AS

	SELECT 
		[CountFail], [LastedLogin], [NhanVienID], [Password], [UserName]
	FROM [dbo].[NhanVienLogin]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVienLogin_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[NhanVienLogin_DeleteByPrimaryKey]
	@NhanVienID bigint
AS

DELETE FROM [dbo].[NhanVienLogin]
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TonGiao]'
GO
CREATE TABLE [dbo].[TonGiao]
(
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenTonGiao] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TonGiaoID] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_TonGiao] on [dbo].[TonGiao]'
GO
ALTER TABLE [dbo].[TonGiao] ADD CONSTRAINT [PK_TonGiao] PRIMARY KEY CLUSTERED  ([TonGiaoID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TonGiao_Insert]'
GO
CREATE PROCEDURE [dbo].[TonGiao_Insert]
	@GhiChu nvarchar(500) = null ,
	@TenTonGiao nvarchar(50) = null ,
	@TonGiaoID int output

AS

INSERT [dbo].[TonGiao]
(
	[GhiChu],
	[TenTonGiao]

)
VALUES
(
	@GhiChu,
	@TenTonGiao

)
	SELECT @TonGiaoID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TonGiao_Update]'
GO
CREATE PROCEDURE [dbo].[TonGiao_Update]
	@GhiChu nvarchar(500) = null,
	@TenTonGiao nvarchar(50) = null,
	@TonGiaoID int

AS

UPDATE [dbo].[TonGiao]
SET
	[GhiChu] = @GhiChu,
	[TenTonGiao] = @TenTonGiao
 WHERE 
	[TonGiaoID] = @TonGiaoID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TonGiao_SelectAll]'
GO
CREATE PROCEDURE [dbo].[TonGiao_SelectAll]
AS

	SELECT 
		[GhiChu], [TenTonGiao], [TonGiaoID]
	FROM [dbo].[TonGiao]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TonGiao_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[TonGiao_DeleteByPrimaryKey]
	@TonGiaoID int
AS

DELETE FROM [dbo].[TonGiao]
 WHERE 
	[TonGiaoID] = @TonGiaoID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KhenThuong]'
GO
CREATE TABLE [dbo].[KhenThuong]
(
[KTID] [bigint] NOT NULL IDENTITY(1, 1),
[NhanVienID] [bigint] NULL,
[HinhThuc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKT] [datetime] NULL,
[LyDo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTien] [bigint] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_KhenThuong] on [dbo].[KhenThuong]'
GO
ALTER TABLE [dbo].[KhenThuong] ADD CONSTRAINT [PK_KhenThuong] PRIMARY KEY CLUSTERED  ([KTID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KhenThuong_Insert]'
GO
CREATE PROCEDURE [dbo].[KhenThuong_Insert]
	@KTID bigint output,
	@NhanVienID bigint = null ,
	@HinhThuc nvarchar(200) = null ,
	@NgayKT datetime = null ,
	@LyDo nvarchar(500) = null ,
	@GhiChu nvarchar(500) = null ,
	@SoTien bigint = null 

AS

INSERT [dbo].[KhenThuong]
(
	[NhanVienID],
	[HinhThuc],
	[NgayKT],
	[LyDo],
	[GhiChu],
	[SoTien]

)
VALUES
(
	@NhanVienID,
	@HinhThuc,
	@NgayKT,
	@LyDo,
	@GhiChu,
	@SoTien

)
	SELECT @KTID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KhenThuong_Update]'
GO
CREATE PROCEDURE [dbo].[KhenThuong_Update]
	@KTID bigint,
	@NhanVienID bigint = null,
	@HinhThuc nvarchar(200) = null,
	@NgayKT datetime = null,
	@LyDo nvarchar(500) = null,
	@GhiChu nvarchar(500) = null,
	@SoTien bigint = null

AS

UPDATE [dbo].[KhenThuong]
SET
	[NhanVienID] = @NhanVienID,
	[HinhThuc] = @HinhThuc,
	[NgayKT] = @NgayKT,
	[LyDo] = @LyDo,
	[GhiChu] = @GhiChu,
	[SoTien] = @SoTien
 WHERE 
	[KTID] = @KTID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KhenThuong_SelectAll]'
GO
CREATE PROCEDURE [dbo].[KhenThuong_SelectAll]
AS

	SELECT 
		[KTID], [NhanVienID], [HinhThuc], [NgayKT], [LyDo], [GhiChu], [SoTien]
	FROM [dbo].[KhenThuong]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KhenThuong_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[KhenThuong_DeleteByPrimaryKey]
	@KTID bigint
AS

DELETE FROM [dbo].[KhenThuong]
 WHERE 
	[KTID] = @KTID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KyLuat]'
GO
CREATE TABLE [dbo].[KyLuat]
(
[KLID] [bigint] NOT NULL IDENTITY(1, 1),
[NhanVienID] [bigint] NULL,
[HinhThuc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKL] [datetime] NULL,
[LyDo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_KyLuat] on [dbo].[KyLuat]'
GO
ALTER TABLE [dbo].[KyLuat] ADD CONSTRAINT [PK_KyLuat] PRIMARY KEY CLUSTERED  ([KLID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KyLuat_Insert]'
GO
CREATE PROCEDURE [dbo].[KyLuat_Insert]
	@KLID bigint output,
	@NhanVienID bigint = null ,
	@HinhThuc nvarchar(200) = null ,
	@NgayKL datetime = null ,
	@LyDo nvarchar(500) = null ,
	@GhiChu nvarchar(500) = null 

AS

INSERT [dbo].[KyLuat]
(
	[NhanVienID],
	[HinhThuc],
	[NgayKL],
	[LyDo],
	[GhiChu]

)
VALUES
(
	@NhanVienID,
	@HinhThuc,
	@NgayKL,
	@LyDo,
	@GhiChu

)
	SELECT @KLID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KyLuat_Update]'
GO
CREATE PROCEDURE [dbo].[KyLuat_Update]
	@KLID bigint,
	@NhanVienID bigint = null,
	@HinhThuc nvarchar(200) = null,
	@NgayKL datetime = null,
	@LyDo nvarchar(500) = null,
	@GhiChu nvarchar(500) = null

AS

UPDATE [dbo].[KyLuat]
SET
	[NhanVienID] = @NhanVienID,
	[HinhThuc] = @HinhThuc,
	[NgayKL] = @NgayKL,
	[LyDo] = @LyDo,
	[GhiChu] = @GhiChu
 WHERE 
	[KLID] = @KLID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KyLuat_SelectAll]'
GO
CREATE PROCEDURE [dbo].[KyLuat_SelectAll]
AS

	SELECT 
		[KLID], [NhanVienID], [HinhThuc], [NgayKL], [LyDo], [GhiChu]
	FROM [dbo].[KyLuat]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KyLuat_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[KyLuat_DeleteByPrimaryKey]
	@KLID bigint
AS

DELETE FROM [dbo].[KyLuat]
 WHERE 
	[KLID] = @KLID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVien_Insert]'
GO
CREATE PROCEDURE [dbo].[NhanVien_Insert]
	@BiDanh nvarchar(200) = null ,
	@ChucVuID int = null ,
	@DonViID int = null ,
	@GhiChu nvarchar(500) = null ,
	@GioiTinh nvarchar(10) = null ,
	@HoDem nvarchar(200) = null ,
	@ImagesPersonal varbinary(1) = null ,
	@MaNV nvarchar(50) = null ,
	@NgaySinh datetime = null ,
	@NhanVienID bigint output,
	@Ten nvarchar(200) = null 

AS

INSERT [dbo].[NhanVien]
(
	[BiDanh],
	[ChucVuID],
	[DonViID],
	[GhiChu],
	[GioiTinh],
	[HoDem],
	[ImagesPersonal],
	[MaNV],
	[NgaySinh],
	[Ten]

)
VALUES
(
	@BiDanh,
	@ChucVuID,
	@DonViID,
	@GhiChu,
	@GioiTinh,
	@HoDem,
	@ImagesPersonal,
	@MaNV,
	@NgaySinh,
	@Ten

)
	SELECT @NhanVienID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVien_Update]'
GO
CREATE PROCEDURE [dbo].[NhanVien_Update]
	@BiDanh nvarchar(200) = null,
	@ChucVuID int = null,
	@DonViID int = null,
	@GhiChu nvarchar(500) = null,
	@GioiTinh nvarchar(10) = null,
	@HoDem nvarchar(200) = null,
	@ImagesPersonal varbinary(1) = null,
	@MaNV nvarchar(50) = null,
	@NgaySinh datetime = null,
	@NhanVienID bigint,
	@Ten nvarchar(200) = null

AS

UPDATE [dbo].[NhanVien]
SET
	[BiDanh] = @BiDanh,
	[ChucVuID] = @ChucVuID,
	[DonViID] = @DonViID,
	[GhiChu] = @GhiChu,
	[GioiTinh] = @GioiTinh,
	[HoDem] = @HoDem,
	[ImagesPersonal] = @ImagesPersonal,
	[MaNV] = @MaNV,
	[NgaySinh] = @NgaySinh,
	[Ten] = @Ten
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVien_SelectAll]'
GO
CREATE PROCEDURE [dbo].[NhanVien_SelectAll]
AS

	SELECT 
		[BiDanh], [ChucVuID], [DonViID], [GhiChu], [GioiTinh], [HoDem], [ImagesPersonal], [MaNV], [NgaySinh], [NhanVienID], [Ten]
	FROM [dbo].[NhanVien]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NhanVien_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[NhanVien_DeleteByPrimaryKey]
	@NhanVienID bigint
AS

DELETE FROM [dbo].[NhanVien]
 WHERE 
	[NhanVienID] = @NhanVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DienBienLuong]'
GO
CREATE TABLE [dbo].[DienBienLuong]
(
[DienBienLuongID] [bigint] NOT NULL IDENTITY(1, 1),
[LuongCoBan] [decimal] (22, 6) NULL,
[LyDo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayApDung] [datetime] NOT NULL,
[NhanVienID] [bigint] NOT NULL,
[PhuCapChucVu] [decimal] (22, 6) NULL,
[PhuCapDocHai] [decimal] (22, 6) NULL,
[NgachLuong] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BacLuong] [int] NULL,
[HSLuong] [decimal] (15, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_DienBienLuong] on [dbo].[DienBienLuong]'
GO
ALTER TABLE [dbo].[DienBienLuong] ADD CONSTRAINT [PK_DienBienLuong] PRIMARY KEY CLUSTERED  ([DienBienLuongID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DienBienLuong_Insert]'
GO
CREATE PROCEDURE [dbo].[DienBienLuong_Insert]
	@DienBienLuongID bigint output,
	@LuongCoBan decimal(22,6) = null ,
	@LyDo nvarchar(500) = null ,
	@NgayApDung datetime ,
	@NhanVienID bigint ,
	@PhuCapChucVu decimal(22,6) = null ,
	@PhuCapDocHai decimal(22,6) = null ,
	@NgachLuong nvarchar(200) = null ,
	@BacLuong int = null ,
	@HSLuong decimal(15,2) = null 

AS

INSERT [dbo].[DienBienLuong]
(
	[LuongCoBan],
	[LyDo],
	[NgayApDung],
	[NhanVienID],
	[PhuCapChucVu],
	[PhuCapDocHai],
	[NgachLuong],
	[BacLuong],
	[HSLuong]

)
VALUES
(
	@LuongCoBan,
	@LyDo,
	@NgayApDung,
	@NhanVienID,
	@PhuCapChucVu,
	@PhuCapDocHai,
	@NgachLuong,
	@BacLuong,
	@HSLuong

)
	SELECT @DienBienLuongID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DienBienLuong_Update]'
GO
CREATE PROCEDURE [dbo].[DienBienLuong_Update]
	@DienBienLuongID bigint,
	@LuongCoBan decimal(22,6) = null,
	@LyDo nvarchar(500) = null,
	@NgayApDung datetime,
	@NhanVienID bigint,
	@PhuCapChucVu decimal(22,6) = null,
	@PhuCapDocHai decimal(22,6) = null,
	@NgachLuong nvarchar(200) = null,
	@BacLuong int = null,
	@HSLuong decimal(15,2) = null

AS

UPDATE [dbo].[DienBienLuong]
SET
	[LuongCoBan] = @LuongCoBan,
	[LyDo] = @LyDo,
	[NgayApDung] = @NgayApDung,
	[NhanVienID] = @NhanVienID,
	[PhuCapChucVu] = @PhuCapChucVu,
	[PhuCapDocHai] = @PhuCapDocHai,
	[NgachLuong] = @NgachLuong,
	[BacLuong] = @BacLuong,
	[HSLuong] = @HSLuong
 WHERE 
	[DienBienLuongID] = @DienBienLuongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DienBienLuong_SelectAll]'
GO
CREATE PROCEDURE [dbo].[DienBienLuong_SelectAll]
AS

	SELECT 
		[DienBienLuongID], [LuongCoBan], [LyDo], [NgayApDung], [NhanVienID], [PhuCapChucVu], [PhuCapDocHai], [NgachLuong], [BacLuong], [HSLuong]
	FROM [dbo].[DienBienLuong]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DienBienLuong_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[DienBienLuong_DeleteByPrimaryKey]
	@DienBienLuongID bigint
AS

DELETE FROM [dbo].[DienBienLuong]
 WHERE 
	[DienBienLuongID] = @DienBienLuongID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "NhanVien"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 208
               Right = 194
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ChucVu"
            Begin Extent = 
               Top = 6
               Left = 232
               Bottom = 114
               Right = 383
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 21
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'NhanVienFullView', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'NhanVienFullView', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO